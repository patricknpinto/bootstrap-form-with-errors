# BsFormWithErrors
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'bs_form_with_errors'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install bs_form_with_errors
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
