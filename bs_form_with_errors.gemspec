# frozen_string_literal: true

require_relative 'lib/bs_form_with_errors/version'

Gem::Specification.new do |spec|
  spec.name        = 'bs_form_with_errors'
  spec.version     = BsFormWithErrors::VERSION
  spec.authors     = ['Patrick Pinto']
  spec.email       = ['hello@patricknpinto.com']
  spec.homepage    = 'http://patricknpinto.com'
  spec.summary     = 'Display Bootstrap form with errors correctly.'
  spec.description = 'Display Bootstrap form with errors correctly.'
  spec.license     = 'MIT'

  spec.required_ruby_version = '>= 2.7'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata['allowed_push_host'] = "Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/patricknpinto/enum_utils'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/patricknpinto/enum_utils'

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_dependency 'rails', '>= 6', '< 8'

  spec.add_development_dependency 'faker', '~> 2.10'
  spec.add_development_dependency 'rspec-rails', '~> 3.9'
  spec.add_development_dependency 'rubocop', '~> 0.79'
  spec.add_development_dependency 'rubocop-rspec', '~> 1.38'
  spec.add_development_dependency 'simplecov', '~> 0.18'

  spec.add_development_dependency 'pry-byebug', '~> 3.8'
  spec.add_development_dependency 'pry-rails', '~> 0.3'
end
