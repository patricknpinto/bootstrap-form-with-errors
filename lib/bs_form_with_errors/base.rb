# frozen_string_literal: false

# Avoid wrapping inputs and labels around a div.field_with_errors element
# and adding bootstrap elements
ActionView::Base.field_error_proc = proc do |html_tag, instance|
  if instance.is_a?(ActionView::Helpers::Tags::Label)
    html_tag
  else
    option = instance.is_a?(ActionView::Helpers::Tags::Select) ? '@html_options' : '@options'
    options = instance.instance_variable_get(option)

    # calling instance.render re-renders the component, so we need to validate id it already
    # was rendered before and avoind entering in an infinite cycle
    options[:class] ||= ''
    if options[:class].include?('is-invalid')
      html_tag
    else
      attribute = instance.instance_variable_get('@method_name')

      options[:class] << ' is-invalid'
      instance.instance_variable_set(option, options)

      if instance.is_a?(ActionView::Helpers::Tags::RadioButton)
        instance.render
      else
        input_html = ApplicationController.helpers.content_tag(
          :div,
          instance.object.errors[attribute].join(', '),
          class: 'invalid-feedback'
        )
        instance.render.concat(input_html)
      end
    end
  end
end
